from mainScript import returnNextCollection
from datetime import datetime
import unittest

# my unit test

class BinCollectionTests(unittest.TestCase):
    def setUp(self):
        pass
    # check that january 1st 2018 returns the expected number of results value for the collection
    def testJanuary1(self):
        date1 = datetime.strptime('01-01-2018', '%d-%m-%Y').date()
        exp_data = len(returnNextCollection(date1))
        self.assertEqual(exp_data, 1)


    #     check that next year returns a null value
    def testFuture(self):
        date2 = datetime.strptime('01-01-2019', '%d-%m-%Y').date()
        exp_data = len(returnNextCollection(date2))
        self.assertEqual(exp_data, 0)
import null as null
from icalendar import Calendar
from datetime import datetime
import requests


url = requests.get('https://s3-eu-west-1.amazonaws.com/fs-downloads/GM/binfeed.ical').content.decode()
cal1 = Calendar.from_ical(url)
# function to get data from user and check if valid date was entered
def userDate():
    isValid=False
    while not isValid:
        userIn = input("Type Date(2018): mm/dd/yy: ")
        try:
            date1 = datetime.strptime(userIn, "%m/%d/%y")
            isValid=True
        except:
            print("Try Again!\n")
    return date1.date()


def returnNextCollection(userInput):
    """
Finds the date of next bin collection after the user inputted datetime and prints the results.
"""
    nextCollection = null
    dayArray = []
    collectionArray = []
    for component in cal1.walk():
        if component.name == "VEVENT":
        # only pass through time information in the future and that is less than the current timedelta
            if ((nextCollection == null ) or (nextCollection >= ((component.get('dtstart').dt) - userInput))) and (component.get('dtstart').dt) >= userInput:
                # check for items on the same date or better as the current closest date as it runs through for loop
                start_date= (component.get('dtstart').dt)
                nextCollection = (component.get('dtstart').dt) - userInput
                dayArray.append(component)

    if nextCollection == null :
        print('No planned collections!')
    else:
        print(nextCollection)
        for item in dayArray:
            if (item.get('dtstart').dt) == start_date:
                print (item.get('dtstart').dt)
                print (item.get('summary'))
                collectionArray.append(item)

    return collectionArray
if __name__ == '__main__':

# retrieving the file and making a ical object

# ask the user for the specified date
    userInputs = userDate()
    returnNextCollection(userInputs)